package services

import (
	"app/app/adapter/session"
	"app/app/adapter/user"
	"app/logger"
	"context"
	"encoding/json"
)

// SessionService :
type SessionService interface {
	GetServiceName() string
	Create(ctx context.Context, request *session.CreateRequest, response *session.CreateResponse) (err error)
}

// UserService :
type UserService interface {
	GetServiceName() string
	Create(ctx context.Context, request *user.CreateRequest, response *user.CreateResponse) (err error)
}

// Log logging service information
func Log(fields logger.Fields, err error) {
	if err != nil {
		logError(fields, err)
		return
	}
	logInfo(fields)
}

func logError(fields logger.Fields, err error) {
	b, errr := json.Marshal(fields)
	if errr != nil {
		return
	}
	logger.Log.Errorf("%s, args: %s", err.Error(), string(b))
}

func logInfo(fields logger.Fields) {
	b, _ := json.Marshal(fields)
	logger.Log.Infof("args: %s", string(b))
}

package user

import (
	"app/app/adapter/user"
	"app/app/model"
	"app/app/repositories"
	"app/app/services"
	"app/errors"
	"app/errors/codes"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
)

type service struct {
	name  string
	repos *repositories.Repositories
}

// NewService returns an instance of services.UserService
func NewService(name string, repos *repositories.Repositories) services.UserService {
	s := &service{name, repos}
	return s
}

func (s *service) GetServiceName() string {
	return s.name
}

func (s *service) Create(ctx context.Context, request *user.CreateRequest, response *user.CreateResponse) (err error) {
	_, err = s.repos.UserRepo.GetByEmail(ctx, request.Email)
	if err != nil {
		if err != mongo.ErrNoDocuments {
			return errors.InternalServerError(codes.Unknown, err.Error())
		}
	} else {
		return errors.BadRequest(codes.InvalidArgument, "this email is already exists")
	}
	hashedPwd, err := bcrypt.GenerateFromPassword([]byte(request.Password), bcrypt.DefaultCost)
	if err != nil {
		return errors.BadRequest(codes.InvalidArgument, "invalid password character")
	}
	now := time.Now()
	user := &model.UserModel{
		FirstName:   request.FirstName,
		LastName:    request.LastName,
		PhoneNumber: request.PhoneNumber,
		Email:       request.Email,
		Password:    hashedPwd,
		CreatedAt:   now,
	}
	if err := s.repos.UserRepo.Insert(ctx, user); err != nil {
		return errors.InternalServerError(codes.Unknown, err.Error())
	}
	response.Id = user.ID.Hex()
	return nil
}

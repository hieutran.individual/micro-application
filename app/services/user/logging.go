package user

import (
	"app/app/adapter/user"
	"app/app/services"
	"app/logger"
	"context"
	"time"
)

type logging struct {
	service services.UserService
}

// NewLoggingService returns logging wrapper of the UserService
func NewLoggingService(service services.UserService) services.UserService {
	l := &logging{service}
	return l
}

func (l *logging) GetServiceName() string {
	return l.service.GetServiceName()
}

func (l *logging) Create(ctx context.Context, request *user.CreateRequest, response *user.CreateResponse) (err error) {
	defer func(begin time.Time) {
		services.Log(logger.Fields{
			"request": logger.Fields{
				"firstName":   request.FirstName,
				"lastName":    request.LastName,
				"phoneNumber": request.PhoneNumber,
				"email":       request.Email,
				"password":    "secure string",
			},
			"response": response,
			"took":     time.Since(begin).Milliseconds(),
		}, err)
	}(time.Now())
	return l.service.Create(ctx, request, response)
}

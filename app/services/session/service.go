package session

import (
	"app/app/adapter/session"
	"app/app/repositories"
	"app/app/services"
	"app/errors"
	"app/errors/codes"
	"app/logger"
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type service struct {
	Name  string
	repos *repositories.Repositories
}

// NewService returns services.SessionService instance. See services package to
// read more about SessionService.
func NewService(name string, repos *repositories.Repositories) services.SessionService {
	return &service{name, repos}
}

func (s *service) GetServiceName() string {
	return s.Name
}

func (s *service) Create(ctx context.Context, request *session.CreateRequest, response *session.CreateResponse) (err error) {
	userID, err := primitive.ObjectIDFromHex(request.GetUserID())
	if err != nil {
		return errors.BadRequest(codes.InvalidArgument, "invalid user id")
	}
	user, err := s.repos.UserRepo.GetByID(ctx, userID)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return errors.BadRequest(codes.NotFound, "user is not found")
		}
		return errors.InternalServerError(codes.Unknown, err.Error())
	}
	logger.Log.Info(user)
	return nil
}

package session

import (
	"app/app/adapter/session"
	"app/app/services"
	"app/logger"
	"context"
	"time"
)

type logging struct {
	service services.SessionService
}

// NewLoggingService returns logging wrapper of the SessionService
func NewLoggingService(service services.SessionService) services.SessionService {
	l := &logging{service}
	return l
}

func (l *logging) GetServiceName() string {
	return l.service.GetServiceName()
}

func (l *logging) Create(ctx context.Context, request *session.CreateRequest, response *session.CreateResponse) (err error) {
	defer func(begin time.Time) {
		services.Log(logger.Fields{
			"request":  request,
			"response": response,
			"took":     time.Since(begin).Milliseconds(),
		}, err)
	}(time.Now())
	return l.service.Create(ctx, request, response)
}

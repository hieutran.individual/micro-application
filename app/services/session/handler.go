package session

import (
	"app/app/adapter/session"
	"app/app/services"
	"app/errors"
	"app/errors/codes"
	"app/handler"
	"net/http"

	"github.com/gorilla/mux"
)

type handlerIns struct {
	service services.SessionService
	utils   *handler.Utils
}

// NewHandler returns an instance of handler.Handler.
func NewHandler(service services.SessionService) handler.Handler {
	h := &handlerIns{service, handler.NewUtils()}
	return h
}

func (h *handlerIns) BindRoutes(r *mux.Router) map[string]string {
	sessionRouter := r.PathPrefix("/session").Subrouter()
	sessionRouter.HandleFunc("/create", h.Create).Methods(http.MethodPost)
	return handler.WalkRouter(sessionRouter, h.service.GetServiceName())
}

func (h *handlerIns) Create(w http.ResponseWriter, r *http.Request) {
	var (
		request  = &session.CreateRequest{}
		response = &session.CreateResponse{}
	)
	defer r.Body.Close()
	if err := h.utils.DecodeJSON(w, r.Body, request); err != nil {
		h.utils.EncodeProblemJSON(w, errors.New(http.StatusBadRequest, codes.FailedPrecondition, err.Error()))
		return
	}
	if err := h.service.Create(r.Context(), request, response); err != nil {
		h.utils.EncodeProblemJSON(w, err)
		return
	}
	h.utils.EncodeJSON(http.StatusOK, w, response)
}

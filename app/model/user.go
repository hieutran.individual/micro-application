package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// UserModel :
type UserModel struct {
	ID          primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	FirstName   string             `json:"firstName" bson:"firstName"`
	LastName    string             `json:"lastName" bson:"lastName"`
	PhoneNumber string             `json:"phoneNumber" bson:"phoneNumber"`
	Email       string             `json:"email" bson:"email"`
	Password    []byte             `json:"password" bson:"password"`
	CreatedAt   time.Time          `json:"createdAt" bson:"createdAt"`
}

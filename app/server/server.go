package server

import (
	"app/config"
	"sync"
)

type server struct {
	conf *config.Conf
	mu   sync.Mutex
}

// Server is the main server that serve any (HTTP or GRPC) requests of clients.
// It also control how ours application works.
type Server interface{}

// NewServer returns an instance of Server.
func NewServer(conf *config.Conf) Server {
	s := &server{
		conf,
		sync.Mutex{},
	}
	if s.conf == nil {
		s.conf = &config.DefaultConf
	}
	return s
}

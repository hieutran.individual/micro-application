package server

// Opt is the option of server. Using it to handle how our application works.
type Opt func(s *server) (err error)

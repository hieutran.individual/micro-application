package mongodb

import (
	"app/app/repositories"

	"go.mongodb.org/mongo-driver/mongo"
)

// Build returns an instance contains all repositories of application.
func Build(db *mongo.Database) *repositories.Repositories {
	repos := repositories.Repositories{
		UserRepo: NewUserRepository(db),
	}
	return &repos
}

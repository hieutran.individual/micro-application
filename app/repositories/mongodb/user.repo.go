package mongodb

import (
	"app/app/model"
	"app/app/repositories"
	"app/logger"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	userCollection = "user"
)

type userRepo struct {
	col *mongo.Collection
}

// NewUserRepository returns an instance of repositories.UserRepository
func NewUserRepository(db *mongo.Database) repositories.UserRepository {
	r := userRepo{col: db.Collection(userCollection)}
	go r.Indexes()
	return &r
}

func (r *userRepo) Indexes() {
	emailIndxOpt := options.Index()
	emailIndxOpt.SetUnique(true)
	emailIndex := mongo.IndexModel{
		Keys: bson.D{
			{
				Key:   "email",
				Value: -1,
			},
		},
		Options: emailIndxOpt,
	}
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	indxName, err := r.col.Indexes().CreateOne(ctx, emailIndex)
	if err != nil {
		logger.Log.Errorf("cannot create index of %s collection", userCollection)
		return
	}
	logger.Log.Infof("index %s of %s collection has been created", indxName, userCollection)
}

func (r *userRepo) GetByID(ctx context.Context, id primitive.ObjectID) (m *model.UserModel, err error) {
	user := &model.UserModel{}
	return user, r.col.FindOne(ctx, bson.M{"_id": id}).Decode(user)
}

func (r *userRepo) Insert(ctx context.Context, m *model.UserModel) (err error) {
	inserted, err := r.col.InsertOne(ctx, m)
	if err != nil {
		return err
	}
	m.ID = inserted.InsertedID.(primitive.ObjectID)
	return nil
}

func (r *userRepo) GetByEmail(ctx context.Context, email string) (m *model.UserModel, err error) {
	user := &model.UserModel{}
	return user, r.col.FindOne(ctx, bson.M{"email": email}).Decode(user)
}

package repositories

import (
	"app/app/model"
	"app/config"
	"context"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Repositories contains all repository interfaces.
type Repositories struct {
	UserRepo UserRepository
}

// UserRepository :
type UserRepository interface {
	GetByID(ctx context.Context, id primitive.ObjectID) (m *model.UserModel, err error)
	Insert(ctx context.Context, m *model.UserModel) (err error)
	GetByEmail(ctx context.Context, email string) (m *model.UserModel, err error)
}

// GetMongoClient :
func GetMongoClient(conf *config.DB) (*mongo.Client, error) {
	opt := options.Client()
	opt.ApplyURI(conf.URI).SetDirect(conf.DirectConnect)
	client, err := mongo.NewClient(opt)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	if err := client.Connect(context.Background()); err != nil {
		return nil, errors.WithStack(err)
	}
	if err := client.Ping(context.Background(), nil); err != nil {
		return nil, errors.WithStack(err)
	}
	return client, nil
}

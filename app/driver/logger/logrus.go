package logger

import (
	"app/config"
	"app/logger"
	"io"
	"os"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
)

// RegisterLogrus register logrus to std Log
func RegisterLogrus(conf *config.Logging) (err error) {
	rusLogger, err := initLogrus(conf)
	if err != nil {
		return errors.Wrap(err, "register logrus")
	}
	logger.SetLogger(rusLogger)
	return nil
}

func initLogrus(conf *config.Logging) (*logrus.Logger, error) {
	logger := logrus.New()
	level, err := logrus.ParseLevel(conf.Level)
	if err != nil {
		return nil, errors.Wrap(err, "init logrus")
	}
	logger.SetLevel(level)
	l := &lumberjack.Logger{
		Filename:   conf.Output,
		MaxSize:    conf.MaxSize,
		MaxBackups: conf.MaxBackups,
		MaxAge:     conf.MaxAge,
		Compress:   conf.Compress,
		LocalTime:  true,
	}
	mw := io.MultiWriter(l, os.Stdout)
	logger.SetOutput(mw)
	switch conf.Encoding {
	case "json":
		logger.SetFormatter(&logrus.JSONFormatter{
			TimestampFormat: conf.TimestampFormat,
		})
	default:
		logger.SetFormatter(&logrus.TextFormatter{
			TimestampFormat: conf.TimestampFormat,
		})
	}
	return logger, nil
}

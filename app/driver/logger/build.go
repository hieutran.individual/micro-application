package logger

import (
	"app/config"
	"app/logger"
	"fmt"

	"github.com/pkg/errors"
)

// Build builds the logging system
func Build(conf *config.Logging) (err error) {
	defer errors.Wrap(err, "build logging")
	if err = conf.Driver.Validate(); err != nil {
		return errors.Wrap(err, "validate logging driver config failed")
	}
	switch conf.Driver {
	case config.LogrusDriver:
		if err = RegisterLogrus(conf); err != nil {
			return errors.Wrap(err, "register logrus driver failed")
		}
		logger.Log.Info("builded logrus logging system")
		return nil
	case config.ZapDriver:
		if err = RegisterLogZap(conf); err != nil {
			return errors.Wrap(err, "register logzap driver failed")
		}
		logger.Log.Info("builded logzap logging system")
		return nil
	default:
		return errors.Wrap(fmt.Errorf("invalid logging driver config"), "build logging")
	}
}

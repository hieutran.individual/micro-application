package logger

import (
	"app/config"
	"app/logger"
	"fmt"
	"net/url"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

// RegisterLogZap register zap logger to std Log
func RegisterLogZap(conf *config.Logging) (err error) {
	zlogger, err := initLogZap(conf)
	if err != nil {
		return errors.Wrap(err, "register logzap")
	}
	defer zlogger.Sync()
	zlog := zlogger.Sugar()
	logger.SetLogger(zlog)
	return nil
}

type lumberjackSink struct {
	*lumberjack.Logger
}

// Sync implements zap.Sink. The remaining methods are implemented
// by the embedded *lumberjack.Logger.
func (lumberjackSink) Sync() error { return nil }

func initLogZap(conf *config.Logging) (*zap.Logger, error) {
	zap.RegisterSink("lumberjack", func(u *url.URL) (zap.Sink, error) {
		return lumberjackSink{
			Logger: &lumberjack.Logger{
				Filename:   conf.Output,
				MaxSize:    conf.MaxSize,
				MaxBackups: conf.MaxBackups,
				MaxAge:     conf.MaxAge,
				Compress:   conf.Compress,
				LocalTime:  true,
			},
		}, nil
	})
	level := zap.AtomicLevel{}
	if err := level.UnmarshalText([]byte(conf.Level)); err != nil {
		return nil, errors.Wrap(err, "init log")
	}
	encoding := "json"
	if conf.Encoding == "text" {
		encoding = "console"
	}
	cfg := zap.Config{
		Level:            level,
		Development:      conf.Development,
		Encoding:         encoding,
		OutputPaths:      []string{"stdout", fmt.Sprintf("lumberjack:%s", conf.Output)},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			TimeKey:          "ts",
			LevelKey:         "level",
			MessageKey:       "msg",
			NameKey:          "name",
			StacktraceKey:    "stacktrace",
			CallerKey:        "caller",
			LineEnding:       "\n",
			EncodeTime:       zapcore.TimeEncoderOfLayout(conf.TimestampFormat),
			EncodeLevel:      zapcore.LowercaseLevelEncoder,
			EncodeDuration:   zapcore.StringDurationEncoder,
			EncodeCaller:     zapcore.ShortCallerEncoder,
			ConsoleSeparator: "\t",
		},
		DisableStacktrace: true,
	}
	zlogger, err := cfg.Build()
	if err != nil {
		return nil, errors.Wrap(err, "init log")
	}
	zlogger.Debug("zap logger construction succeeded")
	return zlogger, nil
}

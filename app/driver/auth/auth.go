package auth

import "app/auth"

type noop struct {
}

// NewAuth returns an instance of auth.Auth package.
func NewAuth() auth.Auth {
	a := noop{}
	return &a
}

func (a *noop) Generate(id string, meta auth.AccountMetadata) auth.Account {
	authAcc := auth.Account{}
	return authAcc
}

package server

import (
	loggerDrv "app/app/driver/logger"
	"app/app/repositories"
	"app/config"
	"app/handler"
	"app/logger"
	"context"
	"net"
	"net/http"
	"sync"

	"app/server"

	"github.com/gorilla/mux"
)

type srv struct {
	conf      *config.Conf
	webServer *webServer
	mu        sync.Mutex
	repos     *repositories.Repositories
	wg        *sync.WaitGroup
}

type webServer struct {
	srv       *http.Server
	router    *mux.Router
	apiRouter *mux.Router
	listener  net.Listener
	handlers  []handler.Handler
}

// NewServer returns an instance of Server.
func NewServer(conf *config.Conf, opts ...Opt) server.Server {
	s := &srv{
		conf,
		nil,
		sync.Mutex{},
		nil,
		&sync.WaitGroup{},
	}
	loggerDrv.Build(&conf.Logging)
	for _, opt := range opts {
		if err := opt(s); err != nil {
			logger.Log.Fatalf("cannot apply option to service: %+v", err)
		}
	}
	logger.Log.Info("server has been established")
	return s
}

func (s *srv) Start() {
	if s.webServer != nil {
		s.wg.Add(1)
		go func() {
			defer s.wg.Done()
			addr := s.webServer.listener.Addr()
			for _, handler := range s.webServer.handlers {
				routes := handler.BindRoutes(s.webServer.apiRouter)
				for path, method := range routes {
					logger.Log.Infof("web server applied route: %s %s%s", method, addr, path)
				}
			}
			logger.Log.Infof("web server is listenning at %s", addr)
			if err := s.webServer.srv.Serve(s.webServer.listener); err != nil {
			}
		}()
	}
	s.wg.Wait()
}

func (s *srv) Shutdown(ctx context.Context) {
	if err := s.webServer.srv.Shutdown(ctx); err != nil {
		logger.Log.Errorf("an error occured while shutting down the server")
	}
}

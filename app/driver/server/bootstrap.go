package server

import (
	"app/app/repositories"
	"app/app/repositories/mongodb"
	"app/app/services/session"
	"app/app/services/user"
	"app/config"
	"app/handler"
	"app/logger"
	"fmt"
	"net"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

// Opt is the option of server. Using it to handle how our application works.
type Opt func(s *srv) (err error)

// SetupWebServer setup application's web server
func SetupWebServer() Opt {
	return func(s *srv) (err error) {
		router := mux.NewRouter()
		apiRouter := router.PathPrefix(s.conf.WebServer.APIPrefix).Subrouter()
		srv := &http.Server{
			Handler: router,
		}
		listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", s.conf.App.Host, s.conf.WebServer.Port))
		if err != nil {
			return errors.WithStack(err)
		}
		s.webServer = &webServer{
			srv,
			router,
			apiRouter,
			listener,
			[]handler.Handler{},
		}
		logger.Log.Info("web server has been established")
		return nil
	}
}

func SetupDAL() Opt {
	return func(s *srv) (err error) {
		switch s.conf.DB.Type {
		case config.MysqlDB:
			return errors.WithStack(fmt.Errorf("unsupported this db now"))
		case config.MongoDB:
			client, err := repositories.GetMongoClient(&s.conf.DB)
			if err != nil {
				return errors.WithStack(err)
			}
			db := client.Database(s.conf.DB.DBName)
			s.repos = mongodb.Build(db)
			logger.Log.Info("database repositories has been established by mongo driver")
		default:
			return errors.WithStack(fmt.Errorf("invalid db"))
		}
		return nil
	}
}

func SetupServices() Opt {
	return func(s *srv) (err error) {
		s.initSessionService("session")
		s.initUserService("user")
		return nil
	}
}

func (s *srv) initUserService(name string) {
	service := user.NewService(name, s.repos)
	service = user.NewLoggingService(service)
	s.webServer.handlers = append(s.webServer.handlers, user.NewHandler(service))
	logger.Log.Infof("%s service has been established", name)
}

func (s *srv) initSessionService(name string) {
	service := session.NewService(name, s.repos)
	service = session.NewLoggingService(service)
	s.webServer.handlers = append(s.webServer.handlers, session.NewHandler(service))
	logger.Log.Infof("%s service has been established", name)
}

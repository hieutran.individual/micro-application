module app

go 1.16

require (
	github.com/golang/protobuf v1.4.3
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.0
	go.mongodb.org/mongo-driver v1.4.6
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.4.0
)

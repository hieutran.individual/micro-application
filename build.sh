#!/bin/bash
BUILD_PATH="`dirname \"$0\"`"
BUILD_PATH="`( cd \"$BUILD_PATH\" && pwd )`"
if [ -z "$BUILD_PATH" ] ; then
  exit 1
fi
echo $BUILD_PATH
go build -race -ldflags "-extldflags '-static'" -o=v1 $BUILD_PATH

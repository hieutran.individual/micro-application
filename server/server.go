package server

import "context"

// Server is the main server that serve any (HTTP or GRPC) requests of clients.
// It also control how ours application works.
type Server interface {
	Start()
	Shutdown(ctx context.Context)
}

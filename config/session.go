package config

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

// Session indicates session configuration of application.
type Session struct {
	// This option allows you to easily specify that all of your session data
	// should be encrypted before it is stored. All encryption will be run
	// automatically by session service and you can use the Session like normal.
	Encrypt bool `yaml:"encrypt" json:"encrypt"`
	// Addrs sets the addresses of auth
	// When using the "mongodb" or "redis" session drivers, you may specify
	// a connection that should be used to manage these sessions. This should
	// correspond to a connection in your database configuration options.
	DB DB `yaml:"db" json:"db"`
	// Here you may change the name of the cookie used to identify a session
	// instance by ID. The name specified here will get used every time a new
	// session cookie is created by the framework for every driver.
	CookieName string `yaml:"cookie_name" json:"cookie_name"`
	// The session cookie path determines the path for which the cookie will be
	// regarded as available. Typically, this will be the root path of your
	// application but you are free to change this when necessary.
	Path string `yaml:"path" json:"path"`
	// Here you may change the domain of the cookie used to identify a session in
	// your application. This will determine which domains the cookie is available
	// to in your application. A sensible default has been set.
	Domain string `yaml:"domain" json:"domain"`
	// By setting this option to true, session cookies will only be sent back to
	// the server if the browser has a HTTPS connection. This will keep the cookie
	// from being sent to you if it can not be done securely.
	Secure bool `yaml:"secure" json:"secure"`
	// Setting this value to true will prevent JavaScript from accessing the value
	// of the cookie and the cookie will only be accessible through the HTTP
	// protocol. You are free to modify this option if needed.
	HTTPOnly bool `yaml:"http_only" json:"http_only"`
	// This option determines how your cookies behave when cross-site requests
	// take place, and can be used to mitigate CSRF attacks. By default, we do not
	// enable this as other CSRF protection services are in place.
	//
	// Supported: "lax", "strict"
	SameSite http.SameSite `yaml:"same_site" json:"same_site"`
	// When using the "mongodb" session connection, you may specify the collection
	// we should use to manage the sessions. Of course, a sensible default is
	// provided for you; however, you are free to change this as needed.
	Collection string `yaml:"collection" json:"collection"`
	// HeaderName is the authorization header name. It is used to store access token
	// of json web token.
	HeaderName string `yaml:"header" json:"header"`
	// The secret is a symmetric key that is known by both the sender and the
	// receiver. It is negotiated and distributed out of band. Hence, if you're
	// the intended recipient of the token, the sender should have provided you
	// with the secret out of band.
	AccessSecret  string `yaml:"access_secret" json:"access_secret"`
	RefreshSecret string `yaml:"refresh_secret" json:"refresh_secret"`
	// https://help.okta.com/en/prod/Content/Topics/Security/API_Access.htm
	// Access Tokens: at least 5 minutes, no more than 24 hours (configurable
	// through Token Inline Hook feature or Access Policies)
	AccessLifeTime int64 `yaml:"access_life_time" json:"access_life_time"`
	// Refresh Tokens: at least 10 minutes, possibly unlimited (configurable
	// through Access Policies)
	RefreshLifeTime int64 `yaml:"refresh_life_time" json:"refresh_life_time"`
}

// Validate validate the Session configuration.
func (s *Session) Validate() (err error) {
	if err = s.DB.Validate(); err != nil {
		return errors.WithStack(err)
	}
	if len(strings.TrimSpace(s.CookieName)) == 0 {
		return errors.WithStack(fmt.Errorf("cookie name is empty"))
	}
	if len(strings.TrimSpace(s.HeaderName)) == 0 {
		return errors.WithStack(fmt.Errorf("header name is empty"))
	}
	if len(strings.TrimSpace(s.AccessSecret)) == 0 || len(strings.TrimSpace(s.RefreshSecret)) == 0 {
		return errors.WithStack(fmt.Errorf("secret key is empty"))
	}
	if s.DB.Type == MongoDB && len(strings.TrimSpace(s.Collection)) == 0 {
		return errors.WithStack(fmt.Errorf("collection name is empty if db type is mongodb"))
	}
	if s.AccessLifeTime < 5 {
		return errors.WithStack(fmt.Errorf("invalid access life time. The access life time is at least 5 minutes"))
	}
	if s.AccessLifeTime > 24*60 {
		return errors.WithStack(fmt.Errorf("invalid access life time. The access life time is no more than 24 hours"))
	}
	if s.RefreshLifeTime < 10 {
		return errors.WithStack(fmt.Errorf("invalid refresh life time. The refresh life time is at least 5 minutes"))
	}
	return nil
}

var (
	// DefaultSession is the default session configuration.
	DefaultSession = Session{
		false,
		DB{
			MongoDB,
			"mongodb://0.0.0.0:27017",
			"app_session",
			true,
		},
		fmt.Sprintf("%s_session", DefaultApp.Name),
		"/",
		"",
		false,
		true,
		http.SameSiteStrictMode,
		"session",
		"authorization",
		"@cc3ss s3cr3t",
		"r3fr3sh s3cr3t",
		15,
		120,
	}
)

package config

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"
)

// DB represents the database configuration.
type DB struct {
	// the Type is the type of DB that you want to connect.
	Type TypeDB `yaml:"type"`
	// The URI can contain host names, IPv4/IPv6 literals, or an SRV record that
	// will be resolved when the Client is created. When using an SRV record, TLS
	// support is implictly enabled. Specify the "tls=false" URI option to
	// override this.
	// If the connection string contains any options that have previously been
	// set, it will overwrite them. Options that correspond to multiple URI
	// parameters, such as WriteConcern, will be completely overwritten if any
	// of the query parameters are specified. If an option is set on ClientOptions
	// after this method is called, that option will override any option applied
	// via the connection string.
	URI string `yaml:"uri" json:"uri"`
	// DBName is the given name configured with the given DatabaseOptions.
	DBName string `yaml:"db_name" json:"db_name"`
	// If the "connect" and "directConnection" URI options are both specified
	// in the connection string, their values must not conflict. Direct
	// connections are not valid if multiple hosts are specified or an SRV URI
	// is used. The default value for this option is false.
	DirectConnect bool `yaml:"direct_connect" json:"direct_connect"`
}

// Validate validate the DB is correct or not.
func (d *DB) Validate() (err error) {
	if err = d.Type.Validate(); err != nil {
		return errors.WithStack(err)
	}
	if len(strings.TrimSpace(d.URI)) == 0 {
		return errors.WithStack(fmt.Errorf("uri connection is empty"))
	}
	if len(strings.TrimSpace(d.DBName)) == 0 {
		return errors.WithStack(fmt.Errorf("db name is empty"))
	}
	return nil
}

// TypeDB is the type of DB that you want to connect.
type TypeDB string

// Validate validate typeDB is correct or not.
func (t TypeDB) Validate() (err error) {
	switch t {
	case MongoDB, MysqlDB:
		return nil
	default:
		return fmt.Errorf("invalid type of db: %s", t)
	}
}

var (
	// MongoDB is the mongo database type.
	MongoDB TypeDB = "mongo"
	// MysqlDB is the mysql database type.
	MysqlDB TypeDB = "mysql"
	// RedisDB is the redis database type.
	RedisDB TypeDB = "redis"
)

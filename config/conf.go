package config

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

// Conf represents the application config
type Conf struct {
	App       App       `yaml:"app" json:"app"`
	Session   Session   `yaml:"session" json:"session"`
	Logging   Logging   `yaml:"logging" json:"logging"`
	DB        DB        `yaml:"db" json:"db"`
	WebServer WebServer `yaml:"web_server" json:"web_server"`
}

// Validate validate Conf is correct or not.
func (c *Conf) Validate() (err error) {
	if err = c.App.Validate(); err != nil {
		return errors.WithStack(err)
	}
	if err = c.Session.Validate(); err != nil {
		return errors.WithStack(err)
	}
	if err = c.Logging.Validate(); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

// DefaultConf is the default configuration of application.
var DefaultConf = Conf{
	DefaultApp,
	DefaultSession,
	DefaultLogging,
	DB{
		MongoDB,
		"mongodb://0.0.0.0:27017",
		"micro_user",
		true,
	},
	DefaultWebServer,
}

// WriteConfig writes conf to file.
func WriteConfig(name string, conf *Conf) (err error) {
	file, err := os.OpenFile(name, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0744)
	if err != nil {
		return errors.WithStack(err)
	}
	defer file.Close()
	stat, err := file.Stat()
	if err != nil {
		return errors.WithStack(err)
	}
	if stat.IsDir() {
		return errors.WithStack(fmt.Errorf("config file '%s' is a directory", name))
	}
	switch true {
	case strings.HasSuffix(name, ".json"):
		if err := json.NewEncoder(file).Encode(conf); err != nil {
			return errors.WithStack(err)
		}
		return nil
	case strings.HasSuffix(name, ".yaml"):
		if err := yaml.NewEncoder(file).Encode(conf); err != nil {
			return errors.WithStack(err)
		}
		return nil
	default:
		return errors.WithStack(fmt.Errorf("unsupported '%s' file", name))
	}
}

// ReadConfig reads the file of the filename (in the same folder) and put it into the AppConfig
func ReadConfig(name string) (*Conf, error) {
	file, err := os.OpenFile(name, os.O_RDONLY, 0744)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	defer file.Close()
	stat, err := file.Stat()
	if err != nil {
		return nil, errors.WithStack(err)
	}
	if stat.IsDir() {
		return nil, errors.WithStack(fmt.Errorf("config file '%s' is a directory", name))
	}
	conf := &Conf{}
	switch true {
	case strings.HasSuffix(name, ".json"):
		if err := json.NewDecoder(file).Decode(conf); err != nil {
			return nil, errors.WithStack(err)
		}
		return conf, nil
	case strings.HasSuffix(name, ".yaml"):
		if err := yaml.NewDecoder(file).Decode(conf); err != nil {
			return nil, errors.WithStack(err)
		}
		return conf, nil
	default:
		return nil, errors.WithStack(fmt.Errorf("unsupported '%s' file", name))
	}
}

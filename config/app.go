// Package config reasd configurations from a YAML file and load them into
// a AppConfig type to save the configuration information for the application.
// Configuration for different environment can be saved in files with different
// suffix, for example [Dev], [Prod]
package config

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"
)

// App represents the application system config
type App struct {
	// This value is the name of your application. This value is used when the
	// application needs to place the application's name in a notification or
	// any other location as required by the application or its packages.
	Name string `yaml:"name" json:"name"`
	// This value determines the "environment" your application is currently
	// running in. This may determine how you prefer to configure various
	// services the application utilizes. Set this in your "conf.yaml" or
	// "conf.json" file.
	Env AppEnv `yaml:"env" json:"env"`
	// When your application is in debug mode, detailed error messages with
	// stack traces will be shown on every error that occurs within your
	// application. If disabled, a simple generic error page is shown.
	Debug bool `yaml:"debug" json:"debug"`
	// This URL is used by the console to properly generate URLs when using
	// the go command line tool. You should set this to the root of
	// your application so that it is used when running app tasks.
	URL  string `yaml:"url" json:"url"`
	Host string `yaml:"host" json:"host"`
	// This URL is used by the console to properly generate URLs when using
	// the go command line tool. You should set this to the root of
	// your application so that it is used when running app tasks.
	AssetURL string `yaml:"asset_url" json:"asset_url"`
	// Here you may specify the default timezone for your application, which
	// will be used by the time packages.
	Timezone string `yaml:"timezone" json:"timezone"`
	// The application locale determines the default locale that will be used
	// by the translation service provider. You are free to set this value
	// to any of the locales which will be supported by the application.
	Locale string `yaml:"locale" json:"locale"`
	// The fallback locale determines the locale to use when the current one
	// is not available. You may change the value to correspond to any of
	// the language folders that are provided through your application.
	FailbackLocale string `yaml:"failback_locale" json:"failback_locale"`
	// This locale will be used by the Faker go library when generating fake
	// data for your database seeds. For example, this will be used to get
	// localized telephone numbers, street address information and more.
	FakerLocale string `yaml:"fake_locale" json:"fake_locale"`
	// This key is used by the application encrypter service and should be set
	// to a random, 32 character string, otherwise these encrypted strings
	// will not be safe. Please do this before deploying an application!
	EncryptionKey string `yaml:"EncryptionKey" json:"encrytionKey"`
	Cipher        string `yaml:"cipher" json:"cipher"`
}

// Validate checks App attributes is usable or not.
func (c *App) Validate() (err error) {
	if strings.TrimSpace(c.Name) == "" {
		return fmt.Errorf("app name is empty")
	}
	if err = c.Env.Validate(); err != nil {
		return errors.WithStack(err)
	}
	if c.Locale == "" && c.FailbackLocale == "" {
		return errors.WithStack(fmt.Errorf("locale & fallback locale are also empty"))
	}
	return nil
}

var (
	// DefaultApp is the the default configuration of App
	DefaultApp = App{
		Name:           "Application",
		Env:            DevelopmentEnv,
		Debug:          true,
		URL:            "http://localhost",
		Host:           "0.0.0.0",
		AssetURL:       "http://localhost/resource",
		Timezone:       "UTC",
		Locale:         "en",
		FailbackLocale: "en",
		FakerLocale:    "en_US",
		EncryptionKey:  "",
		Cipher:         "AES-256-CBC",
	}
)

// AppEnv is the common type of application environments.
type AppEnv string

// Validate check if c is the right env or not.
func (c AppEnv) Validate() (err error) {
	switch c {
	case ProductionEnv, DevelopmentEnv:
		return nil
	default:
		return fmt.Errorf("invalid app env: %s", c)
	}
}

const (
	// ProductionEnv indicates your application is currently running in
	// 'production' environment.
	ProductionEnv AppEnv = "production"
	// DevelopmentEnv indicates your application is currently running in
	// 'development' environment.
	DevelopmentEnv AppEnv = "development"
)

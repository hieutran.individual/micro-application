package config

import (
	"net/http"
	"time"
)

// A WebServer defines parameters for running an HTTP server.
type WebServer struct {
	// Port optionally specifies the TCP address for the server to listen on,
	// in the form "host:port". If empty, ":http" (port 80) is used.
	// The service names are defined in RFC 6335 and assigned by IANA.
	// See net.Dial for details of the address format.
	Port int `yaml:"port" json:"port"`
	// ReadHeaderTimeout is the amount of time allowed to read
	// request headers. The connection's read deadline is reset
	// after reading the headers and the Handler can decide what
	// is considered too slow for the body. If ReadHeaderTimeout
	// is zero, the value of ReadTimeout is used. If both are
	// zero, there is no timeout.
	ReadHeaderTimeout time.Duration `yaml:"read_header_timeout" json:"read_header_timeout"`
	// WriteTimeout is the maximum duration before timing out
	// writes of the response. It is reset whenever a new
	// request's header is read. Like ReadTimeout, it does not
	// let Handlers make decisions on a per-request basis.
	WriteTimeout time.Duration `yaml:"write_timeout" json:"write_timeout"`
	// IdleTimeout is the maximum amount of time to wait for the
	// next request when keep-alives are enabled. If IdleTimeout
	// is zero, the value of ReadTimeout is used. If both are
	// zero, there is no timeout.
	IdleTimeout time.Duration `yaml:"idle_timeout" json:"idle_timeout"`
	// MaxHeaderBytes controls the maximum number of bytes the
	// server will read parsing the request header's keys and
	// values, including the request line. It does not limit the
	// size of the request body.
	// If zero, DefaultMaxHeaderBytes is used.
	MaxHeaderBytes int    `yaml:"max_header_bytes" json:"max_header_bytes"`
	APIPrefix      string `yaml:"api_prefix" json:"api_prefix"`
}

// DefaultWebServer is default WebServer configuration.
var DefaultWebServer = WebServer{
	4000,
	60 * time.Second,
	60 * time.Second,
	60 * time.Second,
	http.DefaultMaxHeaderBytes,
	"/api/v1",
}

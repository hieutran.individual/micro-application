package auth

// Auth provides authentication and authorization
type Auth interface {
	Generate(ID string, metadata AccountMetadata) Account
}

// Account provided by an auth provider
type Account struct {
	// ID of the account e.g. email
	ID       string          `json:"id"`
	Metadata AccountMetadata `json:"metadata"`
}

// AccountMetadata is any other associated metadata
type AccountMetadata map[string]string

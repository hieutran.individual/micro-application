package main

import (
	"app/app/driver/server"
	"app/config"
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/pkg/errors"
)

var (
	defaultConfigFile        = "./config.yaml"
	defaultExampleConfigFile = "./example_config.yaml"
)

func main() {
	cfgFlag := flag.String("cfg", defaultConfigFile, "get config file to parse")
	defaultCfgFlag := flag.Bool("cfg_default", false, "get default config file")
	flag.Parse()
	if *defaultCfgFlag == true {
		if err := config.WriteConfig(defaultExampleConfigFile, &config.DefaultConf); err != nil {
			log.Fatalf("%+v", err)
		}
		errors.WithStack(nil)
		return
	}
	cfg, err := config.ReadConfig(*cfgFlag)
	if err != nil {
		log.Fatalf("%+v", err)
	}
	if err = cfg.Validate(); err != nil {
		log.Fatalf("%+v", err)
	}
	opts := []server.Opt{
		server.SetupWebServer(),
		server.SetupDAL(),
		server.SetupServices(),
	}
	srv := server.NewServer(cfg, opts...)
	go srv.Start()
	//
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	srv.Shutdown(ctx)
	log.Println("shutting down")
	os.Exit(0)
}

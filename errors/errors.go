package errors

import (
	"app/errors/codes"
	"app/logger"
	"encoding/json"
	"fmt"
	"net/http"
	"runtime"
)

// Error is a stacktrace wrapper of Errors
type Error struct {
	error
	caller string
}

func (e *Error) Error() string {
	return fmt.Sprintf("%s, err: %s", e.caller, e.error.Error())
}

func (e *Errors) Error() string {
	b, _ := json.Marshal(e)
	return string(b)
}

// New returns new instance of Error
func New(code int32, id codes.Codes, detail string) (err error) {
	return &Error{
		&Errors{
			Id:     id.String(),
			Code:   code,
			Detail: detail,
			Status: http.StatusText(int(code)),
		},
		Caller(),
	}
}

// Equal tries to compare errors
func Equal(err1 error, err2 error) bool {
	verr1, ok1 := err1.(*Errors)
	verr2, ok2 := err2.(*Errors)
	if ok1 != ok2 {
		return false
	}
	if !ok1 {
		return err1 == err2
	}
	if verr1.Code != verr2.Code {
		return false
	}
	return true
}

// Parse tries to parse a JSON string into an error. If that
// fails, it will set the given string as the error detail.
func Parse(err string) *Errors {
	e := new(Errors)
	errr := json.Unmarshal([]byte(err), e)
	if errr != nil {
		e.Detail = err
		e.Code = http.StatusInternalServerError
		logger.Log.Errorf("invalid error type, it caused HTTP internal server error - code 500")
	}
	return e
}

// FromError try to convert go error to *Error
func FromError(err error) *Errors {
	withStack, ok := err.(*Error)
	if ok && withStack != nil {
		errors, ok := withStack.error.(*Errors)
		if ok && errors != nil {
			return errors
		}
	}
	return Parse(err.Error())
}

// Caller returns the function calling it
func Caller() string {
	pc := make([]uintptr, 1)
	runtime.Callers(3, pc)
	f := runtime.FuncForPC(pc[0])
	_, line := f.FileLine(pc[0])
	return fmt.Sprintf("func: %s, line: %d", f.Name(), line)
}

// BadRequest generates a 400 error.
func BadRequest(id codes.Codes, format string, a ...interface{}) error {
	return &Error{
		&Errors{
			Id:     id.String(),
			Code:   http.StatusBadRequest,
			Detail: fmt.Sprintf(format, a...),
			Status: http.StatusText(http.StatusBadRequest),
		},
		Caller(),
	}
}

// Unauthorized generates a 401 error.
func Unauthorized(id codes.Codes, format string, a ...interface{}) error {
	return &Error{
		&Errors{
			Id:     id.String(),
			Code:   http.StatusUnauthorized,
			Detail: fmt.Sprintf(format, a...),
			Status: http.StatusText(http.StatusUnauthorized),
		},
		Caller(),
	}
}

// Forbidden generates a 403 error.
func Forbidden(id codes.Codes, format string, a ...interface{}) error {
	return &Error{
		&Errors{
			Id:     id.String(),
			Code:   http.StatusForbidden,
			Detail: fmt.Sprintf(format, a...),
			Status: http.StatusText(http.StatusForbidden),
		},
		Caller(),
	}
}

// NotFound generates a 404 error.
func NotFound(id codes.Codes, format string, a ...interface{}) error {
	return &Error{
		&Errors{
			Id:     id.String(),
			Code:   http.StatusNotFound,
			Detail: fmt.Sprintf(format, a...),
			Status: http.StatusText(http.StatusNotFound),
		},
		Caller(),
	}
}

// // MethodNotAllowed generates a 405 error.
// func MethodNotAllowed(id codes.Codes, format string, a ...interface{}) error {
// 	return New(http.StatusMethodNotAllowed, id, fmt.Sprintf(format, a...))
// }

// // Timeout generates a 408 error.
// func Timeout(id codes.Codes, format string, a ...interface{}) error {
// 	return New(http.StatusRequestTimeout, id, fmt.Sprintf(format, a...))
// }

// // Conflict generates a 409 error.
// func Conflict(id codes.Codes, format string, a ...interface{}) error {
// 	return New(http.StatusConflict, id, fmt.Sprintf(format, a...))
// }

// InternalServerError generates a 500 error.
func InternalServerError(id codes.Codes, format string, a ...interface{}) error {
	return &Error{
		&Errors{
			Id:     id.String(),
			Code:   http.StatusInternalServerError,
			Detail: fmt.Sprintf(format, a...),
			Status: http.StatusText(http.StatusInternalServerError),
		},
		Caller(),
	}
}

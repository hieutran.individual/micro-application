package codes

// An Codes is a string error ID as defined in the GRPC and adapter errors.
type Codes string

func (c Codes) String() string {
	return string(c)
}

const (
	// OK is returned on success.
	OK Codes = "OK"
	// Unknown error. An example of where this error may be returned is
	// if a Status value received from another address space belongs to
	// an error-space that is not known in this address space. Also
	// errors raised by APIs that do not return enough error information
	// may be converted to this error.
	Unknown Codes = "UNKNOWN"
	// InvalidArgument indicates client specified an invalid argument.
	// Note that this differs from FailedPrecondition. It indicates arguments
	// that are problematic regardless of the state of the system
	// (e.g., a malformed file name).
	InvalidArgument Codes = "INVALID_ARGUMENT"
	// NotFound means some requested entity (e.g., file or directory) was
	// not found.
	NotFound Codes = "NOT_FOUND"
	// AlreadyExists means an attempt to create an entity failed because one
	// already exists.
	AlreadyExists Codes = "ALREADY_EXISTS"
	// PermissionDenied indicates the caller does not have permission to
	// execute the specified operation. It must not be used for rejections
	// caused by exhausting some resource (use ResourceExhausted
	// instead for those errors). It must not be
	// used if the caller cannot be identified (use Unauthenticated
	// instead for those errors).
	PermissionDenied Codes = "PERMISSION_DENIED"
	// FailedPrecondition indicates operation was rejected because the
	// system is not in a state required for the operation's execution.
	// For example, directory to be deleted may be non-empty, an rmdir
	// operation is applied to a non-directory, etc.
	FailedPrecondition Codes = "FAILED_PRECONDITION"
	// Aborted indicates the operation was aborted, typically due to a
	// concurrency issue like sequencer check failures, transaction aborts,
	// etc.
	Aborted Codes = "ABORTED"
	// OutOfRange means operation was attempted past the valid range.
	// E.g., seeking or reading past end of file.
	//
	// Unlike InvalidArgument, this error indicates a problem that may
	// be fixed if the system state changes. For example, a 32-bit file
	// system will generate InvalidArgument if asked to read at an
	// offset that is not in the range [0,2^32-1], but it will generate
	// OutOfRange if asked to read from an offset past the current
	// file size.
	//
	// There is a fair bit of overlap between FailedPrecondition and
	// OutOfRange. We recommend using OutOfRange (the more specific
	// error) when it applies so that callers who are iterating through
	// a space can easily look for an OutOfRange error to detect when
	// they are done.
	OutOfRange Codes = "OUT_OF_RANGE"
	// DataLoss indicates unrecoverable data loss or corruption.
	DataLoss Codes = "DATA_LOSS"
	// Unauthenticated indicates the request does not have valid
	// authentication credentials for the operation.
	Unauthenticated Codes = "UNAUTHENTICATED"
)

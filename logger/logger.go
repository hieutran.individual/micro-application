// Package logger Requirement:
// Be able to switch to other log library without modifying the business code.
// No direct dependency on any log library.
// Need one global instance of the logger for the whole application, so you can
// change the log configuration in one place and apply it to the whole app.
// Can change logging behavior easily without modifying code, for example, log
// level.
// Be able to change log level dynamically when the programming is running.
//
// Different log libraries provide different features, some are important for
// debugging.
// Logging information that is important ( the following data are required):
// 1. File name and line number
// 2. Method name and caller name
// 3. Message logging level
// 4. Timestamp
// 5. Error stack trace
// 6. Automatically logging each function call with parameters and results
package logger

// Logger represent common interface for logging function
type Logger interface {
	Errorf(format string, args ...interface{})
	Fatalf(format string, args ...interface{})
	Fatal(args ...interface{})
	Infof(format string, args ...interface{})
	Info(args ...interface{})
	Warnf(format string, args ...interface{})
	Debugf(format string, args ...interface{})
	Debug(args ...interface{})
}

// Log is a package level variable, every program should access logging function
// through "Log"
var Log Logger

// SetLogger is the setter for log variable, it should be the only way to assign
// value to log
func SetLogger(newLogger Logger) {
	Log = newLogger
}

// Fields is key value information that you need to log out.
type Fields map[string]interface{}
